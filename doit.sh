#!/bin/bash
set -x

DEST="${CERNKEYTAB_DESTDIR:-/etc-host}"
KEYTAB=$DEST/krb5.keytab
echo "cern-keytab started"
if [[ ! -f "${KEYTAB}" ]] ; then
    SAMACCOUNT=""

    echo "no existing keytab found under ${KEYTAB}... generating new one"
    while [[ -z $SAMACCOUNT ]]
    do
        echo "waiting for node registration in AD..."
		sleep 15
        XMLCURL=$(curl -s -k --local-port 600-700 "https://lxkerbwin.cern.ch/LxKerb.asmx/ResetComputerPasswordTimeCheck?service=host&t=$(date -u +%FT%T)")
        PASSWORD=$(echo "$XMLCURL" | xmllint --xpath "//*[local-name()='keytabfile']/*[local-name()='computerpassword']/text()" -)
        SAMACCOUNT=$(echo "$XMLCURL" | xmllint --xpath "//*[local-name()='keytabfile']/*[local-name()='samaccountname']/text()" -)
        SAMACCOUNT=${SAMACCOUNT//$/}
	done

	echo "waiting 15 seconds before generating keytab..."
	sleep 15
    CERNDC=$(dig -t SRV _kerberos._tcp.cern.ch | grep ^cerndc | awk 'NR==1{print $1}')
    echo "creating new keytab using ${CERNDC} with computer name '${SAMACCOUNT}'"
    i=0
    while [[ ! -f "${KEYTAB}" ]]
    do
        echo "Attempt to create keytab: $i"
        msktutil --verbose -k $KEYTAB --update --dont-expire-password --server $CERNDC --computer-name $SAMACCOUNT --service host --old-account-password $PASSWORD
        ((i++))
        sleep 3s
    done
    echo "keytab created at ${KEYTAB}"
else
    echo "keytab exists at ${KEYTAB}, not generating new one"
fi

if [ ${CERNKEYTAB_NOSLEEP} != "" ]; then
	exit 0
fi

echo "sleeping forever now"
# loop forever to keep the container alive
while true; do sleep 1000; done
