FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
MAINTAINER Ricardo Rocha <ricardo.rocha@cern.ch>

LABEL name="cern-keytab" \
      maintainer="Ricardo Rocha <ricardo.rocha@cern.ch>" \
      license="UNKNOWN" \
      summary="CERN keytab setup container" \
      version="0.1" \
      help="None" \
      architecture="x86_64" \
      atomic.type="system" \
      distribution-scope="public"

RUN yum install -y \
	bind-utils \
	curl \
	krb5-workstation \
	hostname \
	msktutil \
	&& yum clean all

COPY service.template config.json.template manifest.json tmpfiles.template /exports/

ADD krb5.conf /etc/krb5.conf

ADD doit.sh /doit.sh

CMD ["/doit.sh"]
