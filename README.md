> :warning: This repository [has moved](https://gitlab.cern.ch/kubernetes/security/cern-keytab). The new location is a fork and not a repository migration so that old container registry images were kept.

# What's in this image

This image sets up the krb host keytab for a CERN host.

## Atomic

The easiest way to use this is to launch it as an Atomic system container.
```
atomic install --system gitlab-registry.cern.ch/cloud/cern-keytab
systemctl start cern-keytab
```

The keytab will be placed on the host in the usual location - /etc/krb5.keytab.

## Docker

If you prefer to launch it as a regular Docker container, here's the command:
```
docker run --name cern-keytab --rm --net host \
	-v /etc:/etc-host \
	gitlab-registry.cern.ch/cloud/cern-keytab
```

## Tags

We keep both 'qa' and 'latest' tags, used for rolling out upgrades.

# Issues

# Contributing

You are invited to contribute new features and fixes.

[Check here](https://gitlab.cern.ch/cloud/cern-keytab) for the original Dockerfile and repository.

# Authors

Ricardo Rocha [ricardo.rocha@cern.ch](mailto:ricardo.rocha@cern.ch)

